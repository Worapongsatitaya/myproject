from django.db import models

# Create your models here.


class Category(models.Model):
    typeuser = models.CharField(max_length=255)

    def __str__(self):
        return self.typeuser


class User(models.Model):
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    fname = models.CharField(max_length=255)
    lname = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    publish_date = models.DateTimeField('data pulished')
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
            return self.username
